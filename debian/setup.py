import setuptools

setuptools.setup(
    name="stegosaurus", # Replace with your own username
    version="2020.01.09",
    author="Jonathan",
    author_email="jonathan@pocydon.com",
    description="embedding arbitrary payloads in Python bytecode",
    long_description="tool that allows embedding arbitrary payloads in Python bytecode (pyc or pyo) files.",
    long_description_content_type="text/markdown",
    url="https://gitlab.com/twosix-kali/packages1/deepstate",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)

